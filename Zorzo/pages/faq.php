<?php require_once('../components/header.php'); ?>

<main class="main-faq" role="main">

	<section class="main__title faq">
		<div class="container">
			<h2 class="title">Dúvidas</h2>
			<p class="title__sub">Perguntas Frequentes</p>
		</div>
	</section>

	<section class="faq__content">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-12">
					<?php for ($i=1; $i <= 4; $i++): ?>
						<div class="faq__listing">
							<h2>Lorem ipsum dolor sit amet, consectetuer adipiscing elit <span>?</span></h2>

							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
							</p>
						</div>
					<?php endfor; ?>
				</div>

				<div class="col-md-3 col-sm-12">
					<div class="faq__form">
						<h2>
							Ainda com dúvidas?
						</h2>

						<p>Envie sua pergunta!</p>

						<form>
							<input type="text" name="name" placeholder="Nome" />
							<input type="text" name="email" placeholder="E-mail" />
							<textarea name="textarea" placeholder="Escreva sua mensagem" ></textarea>
							<input class="btn btn-primary" type="submit" value="Enviar" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>