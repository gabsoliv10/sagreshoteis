<?php require_once('../components/header.php'); ?>

<main role="main">
	
	<section class="main__banner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="banner_wrapper">
						<div class="banner__description">
							<h2 class="banner__title">Desfrute <br> o que o balneário camboriú <br> tem de melhor</h2>
							<p class="banner__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
						</div>

						<div class="banner__actions">
							<a href="#" class="btn-call-to-book">Reserve agora</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="block__technical-features">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 block__featured-item">
					<i class="icon icon__breakfast"></i>
					<p class="block__subtitle">Um excelente <br> café da manhã</p>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 block__featured-item">
					<i class="icon icon__city"></i>
					<p class="block__subtitle">Região Central <br> de Balneário Camboriú</p>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 block__featured-item">
					<i class="icon icon__beach"></i>
					<p class="block__subtitle">Próximos da praia <br> e um perto do outro</p>
				</div>
			</div>
		</div>
	</section>

	<section class="block__main-features">
		<div class="container">
			<div class="row">
				<div class="main-features__wrapper">
					<div class="col-xs-12 col-md-3">
						<div class="main-features__description">
							<h2 class="block__title">Conheça o que BC tem de melhor</h2>
							<p class="block__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit distinctio veritatis atque placeat praesentium facere.</p>
						</div>
					</div>
					<div class="col-xs-12 col-md-9 no-padding">
						<div class="owl-carousel owl-theme carousel__features">
							<div class="featured__item">
								<figure class="main-features__images">
									<a href=""><img src="../assets/images/home/melhor-de-bc.png" title="O melhor de BC" alt="O melhor de BC"></a>
								</figure>
							</div>

							<div class="featured__item">
								<figure class="main-features__images">
									<a href=""><img src="../assets/images/home/melhor-de-bc.png" title="O melhor de BC" alt="O melhor de BC"></a>
								</figure>
							</div>

							<div class="featured__item">
								<figure class="main-features__images">
									<a href=""><img src="../assets/images/home/melhor-de-bc.png" title="O melhor de BC" alt="O melhor de BC"></a>
								</figure>
							</div>

							<div class="featured__item">
								<figure class="main-features__images">
									<a href=""><img src="../assets/images/home/melhor-de-bc.png" title="O melhor de BC" alt="O melhor de BC"></a>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<aside class="aside__contact-us hidden-xs">
		<a class="aside__image" href="#" title="Converse Conosco">
			<img src="../assets/images/home/banner-middle-home.png" title="Converse Conosco" alt="Converse Conosco">
		</a>
	</aside>

	<section class="block__testimonials align-flex">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="testimonials_wrapper owl-theme owl-carousel">
						<div class="testimonials__item">
							<i class="icon icon__quote"></i>
							<p class="testimonials__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
							<p class="testimonials__author">Eduardo Silveira - Web Designer</p>
						</div>

						<div class="testimonials__item">
							<i class="icon icon__quote"></i>
							<p class="testimonials__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
							<p class="testimonials__author">Eduardo Silveira - Web Designer</p>
						</div>

						<div class="testimonials__item">
							<i class="icon icon__quote"></i>
							<p class="testimonials__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
							<p class="testimonials__author">Eduardo Silveira - Web Designer</p>
						</div>

						<div class="testimonials__item">
							<i class="icon icon__quote"></i>
							<p class="testimonials__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
							<p class="testimonials__author">Eduardo Silveira - Web Designer</p>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="block__blog block__section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">

						<header>
							<h2 class="section__title">Blog</h2>
							<p class="section__subtitle">Motivos para você vir e outros tantos para querer voltar!</p>
						</header>	

						<div class="blog__listing">
							<div class="row">
								<?php for ($i=1; $i <= 4; $i++): ?>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<article class="blog__post">
											<div class="post__info" >
												<a class="post__thumbnail" href="blog_interna.php" title=""><img src="../assets/images/home/blog-home.png" title="" alt=""></a>
											</div>

											<div class="post__details">

												<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
												<div class="post__excerpt">
													<p>
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
													</p>
												</div>

												<div class="section__actions post__actions">
													<a href="blog_interna.php" title="Leia mais" class="btn-read-more">Continuar lendo...</a>
												</div>
											</div>
										</article>
									</div>
								<?php endfor; ?>
							</div>						
						</div>

					</div>
				</div>
			</div>
		</section>

	</main>

	<?php require_once('../components/footer.php'); ?>