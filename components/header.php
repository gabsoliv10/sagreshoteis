<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body>
  <header class="main-header">
    <div class="container">
      <div class="row align-flex">

        <div class="col-xs-2 col-sm-1 hidden-md hidden-lg">
          <a href="javascript:void(0)" class="btn-mobile js-mobile-menu">
            <span></span>
          </a>
        </div>

        <div class="col-xs-8 col-sm-10 col-md-1">
          <figure class="header__logo">
            <a href="" title="">
              <img src="../assets/images/logo.png" title="Página Inicial" alt="Página Inicial">
            </a>
          </figure>
        </div>   

        <div class="col-xs-12 col-sm-12 col-md-4">
          <div class="info__contact">
            <ul class="contact__list">
              <li><a href="tel:+554733673799"><i class="fa fa-phone"></i> +55 (47) 3367-3799</a></li>
              <li><a href="tel:+999660977">+55 (47) 99966-0977</a></li>
            </ul>
          </div>
        </div>   

        <div class="col-xs-12 col-sm-12 col-md-7">
         <ul class="header__nav">
           <li><a href="index.php" title="Página Inicial">Home</a></li>
           <li><a href="hoteis.php" title="Hotéis">Hotéis</a></li>
           <li><a href="blog.php" title="Blog">Blog</a></li>
           <li><a href="politica-de-hospedagem.php" title="Política de Hospedagem">Política de Hospedagem</a></li>
           <li><a href="faq.php" title="">FAQ</a></li>
           <li><a href="parceiros.php" title="">Parceiros</a></li>
           <li><a href="contato.php" title="">Contato</a></li>
           <li>
            <span class="check-in">
              <i class="icon icon__check-in"></i>
              CHECK-IN
            </span>
            <a class="btn btn-book-now" href="#">Reserva</a>
          </li>
        </ul>
      </div>    

    </div>
  </div>
</header>