<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__contact">
		
		<div class="block__section">
			<header>
				<h1 class="section__title">Contato</h1>
			</header>
		</div>

		<div class="container">
			<div class="row no-margin">
				<div class="col-xs-12">
					
					<div class="form__block">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
								<div class="contact__info">
									<header class="section__title">
										<h2><span>Informações de Contato</span></h2>
									</header>

									<ul class="hotel__list">
										<li>
											<h3>Ilha da Madeira</h3>
											<address>Av. Brasil, 1590 - Centro, Balneário Camboriú - SC</address>
											<a href="mailto:reservasilhas@sagreshoteis.com.br">reservasilhas@sagreshoteis.com.br</a>
											<a href="tel:+5544991268869">(47) 99126-8869</a>
										</li>
										<li>
											<h3>Hotel dos Açores</h3>
											<address>Av. Brasil, 1747 - Centro, Balneário Camboriú - SC</address>
											<a href="mailto:reservasacores@sagreshoteis.com.br">reservasacores@sagreshoteis.com.br</a>
											<a href="tel:+5544992491079">(47) 99249-1079</a>
										</li>
										<li>
											<h3>Praia Hotel</h3>
											<address>Av. Central, 477 - Centro, Balneário Camboriú - SC</address>
											<a href="mailto:reservasilhas@sagreshoteis.com.">reservasilhas@sagreshoteis.com.br</a>
											<a href="tel:+5544992257978">(47) 99225-7978</a>
										</li>
									</ul>

									<div class="contact__actions">
										<label for="" aria-labelledby="">Trabalhe conosco</label>
										<a class="btn-curriculum" href="" title="">Anexar Currículo</a>
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 no-padding">								

								<form name="contactForm" class="contact__form" method="POST">

									<header class="section__title">
										<h2><span>Formulário de Contato</span></h2>
									</header>

									<div class="form__fields">
										<div class="row">

											<div class="col-xs-12 col-md-12">
												<label for="nome" aria-labelledby="nome">
													<select name="">
														<option value="ilha_da_madeira">Ilha da Madeira</option>
														<option value="hotel_dos_acores">Hotel dos Açores</option>
														<option value="sangres_praia_hotel">Sangres Praia Hotel</option>
													</select>
												</label>
											</div>

											<div class="col-xs-12 col-md-12">
												<label for="nome" aria-labelledby="nome">
													<input type="text" name="nome" placeholder="Nome">
												</label>
											</div>

											<div class="col-xs-12 col-md-12">
												<label for="email" aria-labelledby="email">
													<input type="email" name="email" placeholder="E-mail">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="mensagem" aria-labelledby="mensagem">
													<textarea name="mensagem" placeholder="Mensagem"></textarea>
												</label>
											</div>
										</div>

										<div class="form__actions">
											<input class="btn btn-primary" type="submit" value="Enviar">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	
	<div class="block__map">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div id="map__contact" class="map__contact"></div>
				</div>
			</div>
		</div>

		<div class="footer_line"></div>

	</div>

</main>

<div class="contact__footer">
<?php require_once('../components/footer.php'); ?>
	
</div>

