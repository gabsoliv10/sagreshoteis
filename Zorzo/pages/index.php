<?php require_once('../components/header.php'); ?>

<main role="main">
	<section class="main__banner">
		<div class="container">
			<ul class="owl-carousel owl-theme main__carousel">
				<li>
					<div class="main__text col-md-6 col-md-offset-1">
						<h2>Lorem ipsum dolor sit amet, consectetuer.</h2>
						<p>Lorem ipsum dolor sit amet, consectetuer.</p>
						<a href="" class="main__link">Saiba Mais</a>
					</div>
					
					<img src="../assets/images/home/banner-home.png" title="" alt="">
				</li>

				<li>
					<div class="main__text col-md-6 col-md-offset-1">
						<h2>Lorem ipsum dolor sit amet, consectetuer.</h2>
						<p>Lorem ipsum dolor sit amet, consectetuer.</p>
						<a href="" class="main__link">Saiba Mais</a>
					</div>
					
					<img src="../assets/images/home/banner-home.png" title="" alt="">
				</li>

				<li>
					<div class="main__text col-md-6 col-md-offset-1">
						<h2>Lorem ipsum dolor sit amet, consectetuer.</h2>
						<p>Lorem ipsum dolor sit amet, consectetuer.</p>
						<a href="" class="main__link">Saiba Mais</a>
					</div>
					
					<img src="../assets/images/home/banner-home.png" title="" alt="">
				</li>
			</ul>
		</div>
	</section>

	<section class="tip">
		<form>
			<h2>Receba dicas de saúde e estética </h2>
			<input type="email" name="email" placeholder="Seu melhor e-mail" />
			<input class="btn btn-primary" type="submit" value="Quero Receber" />
		</form>
	</section>

	<section class="query">
		<div class="content">
			<h2>Agende a sua consulta</h2>
			<a href="">Agendar</a>
		</div>
	</section>

	<section class="hours">
		<div class="container">
			<div class="row">
				<div class="hours__box">

					<div class="col-md-3 col-sm-12">
						<div class="hours__title">
							<h2>Horário de Funcionamento</h2>
						</div>
					</div>

					<div class="col-md-3 col-sm-12">
						<div class="hours__info">
							<h4>Segunda à Sexta</h4>
							<p>08:00 ao 12:00</p>
							<p>14:00 ao 18:45</p>
						</div>
					</div>

					<div class="col-md-3 col-sm-12">
						<div class="hours__emergency">
							<h4>Sábados e <br/> Domingos</h4>
							<p>Atendimento Emergencial</p>
						</div>
					</div>

					<div class="col-md-3 col-sm-12">
						<div class="hours__img">
							<img class="img-responsive" src="../assets/images/home/24h.png" title="24h" alt="24h">
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

	<section class="services">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="title">
						Serviços
					</h2>
					<p class="title__sub">Conheça nossas especialidades</p>

					<ul class="owl-carousel owl-theme services__carousel services__contain">
						<li>
							<a href="" class="services__item estetica">
								<h2><span>Procedimentos</span> Estéticos</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item protese">
								<h2>Próteses</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item implante">
								<h2>Implantes</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item cirurgia">
								<h2>Cirurgias</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item dental">
								<h2>Estética Dental</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item periodontia">
								<h2>Periodontia</h2>
							</a>
						</li>

						<li>
							<a href="" class="services__item periodontia">
								<h2>Periodontia</h2>
							</a>
						</li>
					</ul>

				</div>
			</div>
		</div>
	</section>

	<section class="blog">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="title title--center">Blog</h2>
					<p class="title__sub title__sub--center">Acompanhe dicas para manter o seu sorriso sempre belo e saudável</p>

					<div class="blog__listing">
						<div class="row">
							<div class="col-md-12">
								<article class="blog__post--large">
									<div class="col-lg-4 col-md-12 col-sm-12">
										<a class="blog__thumbnail" href="blog_interna.php">
											<img class="img-responsive" src="../assets/images/home/blog-home.png" title="" alt="">
										</a>
									</div>
									<div class="col-md-12 col-lg-8 col-sm-12">
										<a class="blog__thumbnail" href="blog_interna.php">
											<h3 class="blog__title blog__title--large">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
											</h3>
											<p class="blog__author">
												Por: Gilberto Zorzo
											</p>

											<p class="blog__text blog__text--large">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
											</p>
										</a>
										<a class="blog__link blog__link--large" href="blog_interna.php">Continuar Lendo >></a>
									</div>
								</article>
							</div>
							<div class="col-md-12 blog__content">
									<div class="col-xs-12 col-sm-6 col-md-5">
										<article class="blog__post">
											<h3 class="blog__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
												
											<p class="blog__text">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat...
											</p>

											<a href="blog_interna.php" title="Leia mais" class="blog__link">Continuar lendo >></a>
										</article>
									</div>

									<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-2">
										<article class="blog__post">
											<h3 class="blog__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
												
											<p class="blog__text">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat...
											</p>

											<a href="blog_interna.php" title="Leia mais" class="blog__link">Continuar lendo >></a>
										</article>
									</div>
							</div>
						</div>						
					</div>
				</div>
				<div class="col-xs-12 text-center">
					<a class="blog__link-page" href="blog.php">Veja Mais Posts</a>
				</div>
			</div>
		</div>
	</section>

	</main>

	<?php require_once('../components/footer.php'); ?>