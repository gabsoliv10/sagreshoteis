<?php require_once('../components/header.php'); ?>

<main class="main-blog-interna" role="main">

	<section class="main__title blog-interna">
		<div class="container">
			<h2 class="title">Blog</h2>
			<p class="title__sub">Acompanhe dicas para manter o seu sorriso sempre belo e saudável</p>
		</div>
	</section>

	<section class="blog-interna__content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="blog-interna__listing">
						<?php for ($i=1; $i <= 5; $i++): ?>
						<article class="blog__post--large clearfix">
							<div class="col-md-5 col-sm-12">
								<a class="blog__thumbnail" href="blog_interna.php">
									<img class="img-responsive" src="../assets/images/blog/blog-interna.png" title="" alt="">
								</a>
							</div>
							<div class="col-md-7 col-sm-12">
								<a class="blog__thumbnail" href="blog_interna.php">
									<h3 class="blog__title blog__title--large">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
									</h3>
									<p class="blog__author">
										Por: Gilberto Zorzo
									</p>

									<p class="blog__text blog__text--large">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
									</p>
								</a>
								<a class="blog__link blog__link--large" href="blog_interna.php">Continuar Lendo >></a>
							</div>
						</article>
						<?php endfor; ?>

						<nav>
							<ul class="pagination">
								<li class="page-item previous"><a class="page-link" href="#"><< Página Anterior</a></li>
								<li class="page-item"><a class="page-link active" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item next"><a class="page-link" href="#">Próxima Página >></a></li>
							</ul>
						</nav>
					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="blog-interna__form">
						<form>
							<p>Assine nossos<br/> posts.</p>
							
							<input type="text" name="email" placeholder="Seu melhor e-mail" />
							<input class="btn btn-primary" type="submit" value="Quero receber" />
						</form>
					</div>

					<div class="blog-interna__pop">
						<h2 class="title title--small">Posts mais Lidos</h2>

						<article class="blog__post--large clearfix">
							<a class="blog__thumbnail" href="blog_interna.php">
								<h3 class="blog__title blog__title">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
								</h3>
								<p class="blog__author">
									Por: Gilberto Zorzo
								</p>
								<p class="blog__text blog__text">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
								</p>
							</a>
							<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
						</article>

						<article class="blog__post--large clearfix">
							<a class="blog__thumbnail" href="blog_interna.php">
								<h3 class="blog__title blog__title">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
								</h3>
								<p class="blog__author">
									Por: Gilberto Zorzo
								</p>
								<p class="blog__text blog__text">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
								</p>
							</a>
							<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
						</article>
					</div>

					<div class="blog-interna__category">
						<h2 class="title title--small">Categorias</h2>

						<ul>
							<li>
								<a href="" class="active">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>