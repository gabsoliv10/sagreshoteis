<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body>
  <header class="main-header">
    <div class="container">
      <div class="row align-flex">

        <div class="col-xs-2 col-sm-1 hidden-md hidden-lg">
          <a href="javascript:void(0)" class="btn-mobile js-mobile-menu">
            <span></span>
          </a>
        </div>

        <div class="col-xs-8 col-sm-10 col-md-3">
          <figure class="header__logo">
            <a href="" title="">
              <img class="img-responsive" src="../assets/images/logo.png" title="Página Inicial" alt="Página Inicial">
            </a>
          </figure>
        </div>   

        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-1">
         <ul class="header__nav">
            <li><a href="tratamento.php" title="Tratamentos">Tratamentos</a></li>
            <li><a href="equipe.php" title="Equipe">Equipe</a></li>
            <li><a href="paciente-em-viagem.php" title="Pacientes em Viagens">Paciente em Viagens</a></li>
            <li><a href="faq.php" title="Dúvidas">Dúvidas</a></li>
            <li class="dropdown">
              <a>A Clínica</a>
              <div class="dropdown-content">
                  <ul>
                    <li><a href="clinica-quem-somos.php">Quem somos</a></li>
                    <li><a href="clinica-estrutura.php">Estrutura</a></li>
                    <li><a href="clinica-tecnologia.php">Tecnlogia</a></li>
                    <li><a href="clinica-bioseguranca.php">Biosegurança</a></li>
                  </ul>
              </div>
            </li>
            <li><a href="contato.php" title="Contato">Contato</a></li>
        </ul>
        <div class="header__social">
          <a href="" target="_blank" title="Facebook" ><i class="fa fa-facebook"></i></a>
          <a href="" target="_blank" title="Youtube"><i class="fa fa-youtube-play"></i></a>
        </div>
      </div>    

    </div>
  </div>
</header>