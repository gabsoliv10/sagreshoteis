<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="blog">

		<div class="blog__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">

						<div class="blog__listing">
							<div class="row">
								<div class="col-xs-12">

									<?php for ($i=1; $i < 3; $i++): ?>
										<article class="blog__post">
											<div class="post__details">

												<figure class="post__thumbnail">
													<img src="../assets/images/blog/blog-interna.png" title="" alt="">
												</figure>


												<div class="post__info-wrapper">

													<div class="post__actions">
														<a title="" href="">
															<i class="icon icon__share"></i>
														</a>
													</div>

													<div class="post__info">
														<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
														<div class="post__excerpt">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
															</p>
														</div>

														<a href="blog_interna.php" title="Leia mais" class="btn-read-more">Continuar Lendo...</a>
													</div>
													
												</div>
											</div>

										</article>
									<?php endfor; ?>

									<div class="block__paginator">
										<div class="paginator">
											<ul>
												<li class="paginator__item is-active"><a  href="" title="">1</a></li>
												<li class="paginator__item"><a href="" title="">2</a></li>
												<li class="paginator__item"><a href="" title=""><i class="icon icon__angle-left--small"></i></a></li>
											</ul>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>

					<div class="col-xs-12 col-md-3">

						<div class="aside__group">
							<aside class="block__search">
								<form class="search__form" name="" action="">
									<label for="search" aria-labelledby="search">
										<input type="text" name="search" placeholder="Pesquisar">
									</label>
									<button type="submit" class="btn-search">
										<span class="screen-readers">Pesquisar</span>
									</button>
								</form>
							</aside>

							<aside class="block__most-readed block__aside">
								<h3>Posts mais lidos</h3>

								<div class="most-readed__listing">
									<?php for ($i=1; $i <= 3; $i++): ?>
										<article class="most-readed__list-item">

											<div class="post__details">
												<figure class="post__thumbnail">
													<img src="https://loremflickr.com/105/70?random=1" title="" alt="">
												</figure>

												<h4 class="post__title"><a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h4>
											</div>

										</article>	
									<?php endfor; ?>
								</div>
							</aside>

							<aside class="block__newsletter block__aside">
								<h3>Assine Nossa News</h3>

								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, dolore, culpa. Voluptas sequi amet ab temporibus quos.</p>

								<form class="search__form" name="" action="">
									<label for="email_newsletter" aria-labelledby="email_newsletter">
										<input type="text" name="email_newsletter" placeholder="Seu e-mail">
									</label>
									<button type="submit" class="btn btn-primary">Assinar</button>
								</form>
							</aside>

							<aside class="block__facebook-plugin"></aside>

							<aside class="block__categories block__aside">
								<h3>Categoria</h3>

								<ul class="category__listing">
									<?php for ($i=1; $i <= 4; $i++): ?> 
										<li>
											<h4>
												<a href="categoria_interna.php" title="">Categoria</a>
											</h4>
										</li>
									<?php endfor; ?>
								</ul>
							</aside>
						</div>	

					</div>

				</div>

			</div>

		</div>
	</section>

</main>

<?php require_once('../components/footer.php'); ?>