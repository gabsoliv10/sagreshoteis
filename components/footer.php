<footer class="main__footer">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-5">

				<div class="footer__block-1">
					<div class="block__social">
						<ul class="social__listing">
							<li>
								<a href="" title="Sindicato no Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="" title="Sindicato no Twitter">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="" title="Sindicato no Instagram">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>

					<div class="block__newsletter">
						<strong class="block__title">Conecte-se com a gente</strong>
						<p class="block__subtitle">Receba em primeira mão as promoções e dicas que movimentam a cidade.</p>

						<form class="form__newsletter" name="" action="" method="POST">
							<label for="newsletter_email" aria-labelledby="newsletter_email">
								<input type="email" name="newsletter_email" placeholder="Seu e-mail">
							</label>

							<button class="btn btn-primary btn-newsletter" type="submit">Enviar</button>
						</form>
					</div>
				</div>

			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-2 trip-advisor">
				<div class="footer__block-2">
					<a href="" title="">
						<img src="../assets/images/trip-advisor.jpg" title="Recomendado por Trip Advisor" alt="Recomendado por Trip Advisor">
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-5">
				<div class="footer__block-3">
					<div class="block__hotels">

						<div class="hotel__list row ">
							<div class="col-xs-12 col-sm-6 col-md-6 hotel__item">
								<h2>Hotel Ilha da Madeira</h2>

								<ul class="info__contact">
									<li>
										<i class="fa fa-whatsapp"></i>
										<a href="tel:+5547991268869" title="">(47) 99126-8869</a>
									</li>
									<li class="click-to-call">
										<a href="" title="">Para atendimento <br> a um click</a>
									</li>
									<li>
										<i class="fa fa-phone"></i>
										<a href="tel:+554721040116" title="">(47) 2104-0116</a>
									</li>
									<li>
										<i class="fa fa-envelope"></i>
										<a href="mailto:reservasilha@sagreshoteis.com.br" title="">reservasilha@sagreshoteis.com.br</a>
									</li>
								</ul>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6 hotel__item">
								<h2>Hotel dos Açores</h2>

								<ul class="info__contact">
									<li>
										<i class="fa fa-whatsapp"></i>
										<a href="tel:+992491079" title="">(47) 99249-1079</a>
									</li>
									<li class="click-to-call">
										<a href="" title="">Para atendimento <br> a um click</a>
									</li>
									<li>
										<i class="fa fa-phone"></i>
										<a href="tel:+554733671616" title="">(47) 3367-1616</a>
									</li>
									<li>
										<i class="fa fa-envelope"></i>
										<a href="mailto:reservasacores@sagreshoteis.com.br" title="">reservasacores@sagreshoteis.com.br</a>
									</li>
								</ul>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6 hotel__item">
								<h2>Sagres Praia Hotel</h2>

								<ul class="info__contact">
									<li>
										<i class="fa fa-whatsapp"></i>
										<a href="tel:+992491079" title="">(47) 99249-1079</a>
									</li>
									<li class="click-to-call">
										<a href="" title="">Para atendimento <br> a um click</a>
									</li>
									<li>
										<i class="fa fa-phone"></i>
										<a href="tel:+554721040116" title="">(47) 3367-0288</a>
									</li>
									<li>
										<i class="fa fa-envelope"></i>
										<a href="mailto:reservassagres@sagreshoteis.com.br" title="">reservassagres@sagreshoteis.com.br</a>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>

		<div class="row">
			
			<div class="footer__nav">
				<nav>
					<a href="" title="">Sobre nós</a>
					<a href="" title="">Termos de Uso</a>
					<a href="" title="">Política de Privacidade</a>
					<a href="" title="">Trabalhe Conosco</a>
				</nav>
			</div>

		</div>
	</div>
</footer>

<script async defer src="https://use.fontawesome.com/4d634eeb4a.js"></script>
<script src="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyC14uhCgN884EhsnAmONv6C41fqhts9Zko
&center=-33.87473063764475,151.0138259246945&zoom=16&format=png&maptype=roadmap&style=color:0xfdf5e8&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels%7Cvisibility:off&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:administrative.neighborhood%7Cvisibility:off&style=feature:administrative.neighborhood%7Celement:geometry%7Ccolor:0xfdf5e8&style=feature:administrative.neighborhood%7Celement:geometry.fill%7Ccolor:0x74bac7%7Cvisibility:simplified&style=feature:poi%7Cvisibility:off&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit%7Cvisibility:off&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x360"></script>
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/app.js"></script>

</body>
</html>