<footer class="footer">
	<div class="footer__social">
		<a href="" target="_blank">
			<i class="fa fa-facebook"></i>
		</a>

		<a href="" target="_blank">
			<i class="fa fa-youtube-play"></i>
		</a>
	</div>
	<div class="container">
		<div class="row">

			<div class="col-md-3 col-sm-12">
				<div class="footer__info">
					<h2 class="footer__title">
						Como Chegar
					</h2>

					<div class="footer__item">
						<i class="fa fa-map-marker"></i>
						<p>
							Rua Irmão Joaquim, 68 Centro – <br/> Florianópolis (SC) 88020-620
						</p>
					</div>

					<div class="footer__item">
						<i class="fa fa-phone fa-flip-horizontal"></i>
						<p>
							<a href="tel:+554832220061">(48) 3222-0061</a>  | <a href="tel:+554899830553"> (48) 9983-0553</a>
						</p>
					</div>

					<div class="footer__item">
						<i class="fa fa-whatsapp"></i>
						<p>
							<a href="tel:+554899830553">(48) 9983-0553</a>
						</p>
					</div>

					<div class="footer__item">
						<i class="fa fa-envelope"></i>
						<p>
							<a href="mailto:clinica@gilbertozorzo.com.br">clinica@gilbertozorzo.com.br</a>
						</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-md-offset-4 col-sm-12 text-center">
				<img class="img-responsive" src="../assets/images/footer/24h.png" title="" alt="">
			</div>
		</div>
	</div>
</footer>
<div class="copyright">
	<a href="http://codde.com.br/" target="_blank">
		<img class="img-responsive center-block" src="../assets/images/footer/copyright.png" title="" alt="">
	</a>
</div>

<script async defer src="https://use.fontawesome.com/4d634eeb4a.js"></script>
<script src="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyC14uhCgN884EhsnAmONv6C41fqhts9Zko
&center=-33.87473063764475,151.0138259246945&zoom=16&format=png&maptype=roadmap&style=color:0xfdf5e8&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels%7Cvisibility:off&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:administrative.neighborhood%7Cvisibility:off&style=feature:administrative.neighborhood%7Celement:geometry%7Ccolor:0xfdf5e8&style=feature:administrative.neighborhood%7Celement:geometry.fill%7Ccolor:0x74bac7%7Cvisibility:simplified&style=feature:poi%7Cvisibility:off&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit%7Cvisibility:off&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x360"></script>
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/app.js"></script>

</body>
</html>