<?php require_once('../components/header.php'); ?>

<main class="main-equipe" role="main">

	<section class="main__title equipe">
		<div class="container">
			<h2 class="title">Equipe</h2>
			<p class="title__sub">Saiba quem somos</p>
		</div>
	</section>

	<section class="equipe__content">
		<div class="container">
			<div class="row">
				<div class="TabControl">
					<div id="header">
						<ul class="abas">
							<li>
								<div class="aba">
									<div class="abas__item one">
										<h2>Dr. Gilberto Zorzo</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item two">
										<h2>Dr. André Nunes Trindade</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item three">
										<h2>Dr. Carmênio de Almeida Mascarenhas</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item four">
										<h2>Dra. Cibele Barreto Correa</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item five">
										<h2>Dr. João Vitor Carneiro de Moura Rocha</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item six">
										<h2>Dr. Marcelo Rodrigues da Silva Nunes</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item seven">
										<h2>Dra. Monique Freiberger Fernandes</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="aba">
									<div class="abas__item eight">
										<h2>Dr. Renato Leal Machado</h2>
									</div>
								</div>
							</li>
						</ul>
					</div>

					<div id="content">
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
										TAB 1 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 2 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 3 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 4 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 5 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 6 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 7 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="conteudo">
							<div class="col-xs-12 equipe__border">
								<div class="col-xs-12">
									<h2 class="equipe__name">
									TAB 8 Dr. André Nunes Trindade
										<span>CRO/SC 5049</span>
									</h2>
								</div>

								<div class="col-md-5 col-sm-12">
									<div class="equipe__text">
										<p>
											Cirurgião Dentista graduado pela UFSC em 1997, especialista em Dentística Restauradora em 2000 pela UFSC, especialista em Implantodontia em 2013 pela ABO-SC, pós-graduado em Cirurgia Avançada em 2001 pela UFSC, em Cirurgia Periodontal em 2015 pela Zenith, em Excelência em Implante pela CIPO em 2015, especialista em Implantodontia em 2013 pela ABO-SC.
										</p>
									</div>
								</div>

								<div class="col-md-6 col-md-offset-1 col-sm-12">
									<div class="equipe__info">
										<h2>Especialidade</h2>
										<p>Implantodontista e Dentística Restauradora</p>

										<div class="equipe__consulta">
											<p>Agende a sua Consulta</p>
											<a href="" class="equipe__link">Agendar</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>