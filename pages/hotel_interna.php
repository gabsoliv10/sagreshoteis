<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="hotel__internal">

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">

					<div class="hotel__main-slider">
						<ul class="owl-theme owl-carousel hotel__carousel">
							<?php for ($i=0; $i <= 2; $i++): ?>
								<li>
									<img src="https://loremflickr.com/1190/700/?random=1" title="" alt="">
								</li>
							<?php endfor; ?>
						</ul>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="hotel__details">
					<div class="col-xs-12 col-sm-12 col-md-7">
						<div class="hotel__info">
							<h2 class="hotel__title">Ilha da Madeira</h2>
							<div class="hotel__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus provident, repellendus eius nesciunt optio vero aut adipisci</div>

							<div class="hotel__description">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius obcaecati voluptate dolorem asperiores. Voluptas enim similique necessitatibus tempora facilis. Sed obcaecati recusandae aspernatur debitis maxime, porro reiciendis optio placeat earum.
								</p>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, earum ratione cum explicabo odit incidunt illo laudantium assumenda, quos ullam error excepturi esse perferendis minus accusantium! Quidem est in molestias.
								</p>
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5">
						<div class="hotel__call-action">
							<h3>
								<i class="icon icon__calendar"></i>
								Faça sua reserva
							</h3>

							<form class="" action="">
								<fieldset class="check__options">
									<label for="check_in" aria-labelledby="check_in">
										<input type="text" name="check_in" placeholder="Check-in">
									</label>

									<label for="check_out" aria-labelledby="check_out">
										<input type="text" name="check_out" placeholder="Check-out">
									</label>
								</fieldset>

								<fieldset class="qty_options">

									<label for="qty_quartos" aria-labelledby="qty_quartos">
										<input type="text" name="qty_quartos" placeholder="" max-length="2">
										<span class="label__name">Quartos</span>
									</label>

									<label for="qty_adultos" aria-labelledby="qty_adultos">
										<input type="text" name="qty_adultos" placeholder="" max-length="2">
										<span class="label__name">Adultos</span>

									</label>

									<label for="qty_criancas" aria-labelledby="qty_criancas">
										<input type="text" name="qty_criancas" placeholder="" max-length="2">
										<span class="label__name">Crianças</span>
									</label>

								</fieldset>

								<button class="btn btn-submit" type="submit">Reservar agora</button>
							</form>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="block__main-features">
						<div class="main-features__wrapper">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<div class="main-features__description">
									<h2 class="block__title">Quartos e acomodações do hotel Açores</h2>
									<p class="block__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit distinctio veritatis atque placeat praesentium facere.</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">
								<ul class="owl-theme owl-carousel carousel__hotel-features">
									<?php for ($i=0; $i <= 2; $i++): ?>
										<li>
											<img src="https://loremflickr.com/1190/510/?random=1" title="" alt="">

											<div class="carousel__content">
												<span>Quarto 1</span>
											</div>
										</li>
									<?php endfor; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">

					<div class="block__technical-features">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__wi-fi"></i>
								<p class="block__subtitle">Wifi</p>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__air"></i>
								<p class="block__subtitle">Ar-condicionado</p>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__tv"></i>
								<p class="block__subtitle">Televisão</p>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__minibar"></i>
								<p class="block__subtitle">Frigobar</p>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__phone"></i>
								<p class="block__subtitle">Telefone</p>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2 block__featured-item">
								<i class="icon icon__coffer"></i>
								<p class="block__subtitle">Cofre</p>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">

					<div class="main__banner">


						<div class="owl-theme owl-carousel internal__hotel-carousel">
							<div class="banner_wrapper">
								<div class="banner__description">
									<h2 class="banner__title">Piscina e café da manhã</h2>
									<p class="banner__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
								</div>

							</div>

							<div class="banner_wrapper">
								<div class="banner__description">
									<h2 class="banner__title">Piscina e café da manhã</h2>
									<p class="banner__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
								</div>

							</div>

							<div class="banner_wrapper">
								<div class="banner__description">
									<h2 class="banner__title">Piscina e café da manhã</h2>
									<p class="banner__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, rerum possimus adipisci minima.</p>
								</div>

							</div>
						</div>

					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="hotel__call-action full--version">
						<h3>
							<i class="icon icon__calendar"></i>
							Faça sua reserva
						</h3>

						<form class="" action="">

							<div class="row">
								<div class="col-xs-12col-xs-12  col-md-5">
									<fieldset class="check__options">
										<label for="check_in" aria-labelledby="check_in">
											<input type="text" name="check_in" placeholder="Check-in">
										</label>

										<label for="check_out" aria-labelledby="check_out">
											<input type="text" name="check_out" placeholder="Check-out">
										</label>
									</fieldset>
								</div>

								<div class="col-xs-12 col-xs-12 col-md-5">
									<fieldset class="qty_options">

										<label for="qty_quartos" aria-labelledby="qty_quartos">
											<input type="text" name="qty_quartos" placeholder="" max-length="2">
											<span class="label__name">Quartos</span>
										</label>

										<label for="qty_adultos" aria-labelledby="qty_adultos">
											<input type="text" name="qty_adultos" placeholder="" max-length="2">
											<span class="label__name">Adultos</span>

										</label>

										<label for="qty_criancas" aria-labelledby="qty_criancas">
											<input type="text" name="qty_criancas" placeholder="" max-length="2">
											<span class="label__name">Crianças</span>
										</label>

									</fieldset>
								</div>

								<div class="col-xs-12 col-md-2">
									<button class="btn btn-submit" type="submit">Reservar agora</button>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="map__container"></div>
				</div>
			</div>
		</div>
	</section>

</main>

<?php require_once('../components/footer.php'); ?>