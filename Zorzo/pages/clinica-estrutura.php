<?php require_once('../components/header.php'); ?>

<main class="main-clinica" role="main">

	<section class="main__title clinica">
		<div class="container">
			<h2 class="title">A Clínica</h2>
			<p class="title__sub">Conheça o que fazemos</p>
		</div>
	</section>

	<section class="clinica__content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="clinica__listing">
						<h2>Estrutura</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>

						<img class="img-responsive" src="../assets/images/clinica/imgs.png" title="Estrutura" alt="Estrutura">
						
						<div class="clinica__action">
							<a href="estrutura.php">
								<img class="img-responsive" src="../assets/images/clinica/estrutura.png" title="Estrutura" alt="Estrutura">
							</a>
							
							<a href="tecnologia.php">
								<img class="img-responsive" src="../assets/images/clinica/tecnologia.png" title="tecnologia" alt="tecnologia">
							</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">
					<h2 class="title title--small">Conheça nossas Especialidades</h2>

					<div class="clinica__menu">
						<div class="clinica__menu-link estetica">
							<a href="procedimento-estetico.php">
								Proedimentos <br> estéticos
							</a>
						</div>
						<div class="clinica__menu-link protese">
							<a href="protese.php">
								Próteses
							</a>
						</div>
						<div class="clinica__menu-link implante">
							<a href="implante.php">
								Implantes
							</a>
						</div>
						<div class="clinica__menu-link cirurgia">
							<a href="cirurgia.php">
								Cirurgias
							</a>
						</div>
						<div class="clinica__menu-link dental">
							<a href="estetica-dental.php">
								ESTÉTICA DENTAL
							</a>
						</div>
						<div class="clinica__menu-link periodontia">
							<a href="periodontia.php">
								PERIODONTIA
							</a>
						</div>
						<div class="clinica__menu-link endodontia">
							<a href="endodontia.php">
								ENDODONTIA
							</a>
						</div>
						<div class="clinica__menu-link urgencia">
							<a href="urgencias.php">
								URGÊNCIAS
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>