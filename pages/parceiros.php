<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__partners">
		<div class="container">
			<div class="row">
				<?php for ($i=1; $i <= 8; $i++): ?>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<article class="partner">
							<figure class="partner__logo">
								<img src="../assets/images/logo-partners.png" title="Logo do Parceiro" alt="Parceiro">
							</figure>
							<div class="partner__info">
								<h3 class="partner__title">Nome do Parceiro</h3>
								<div class="partner__description">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
									</p>
								</div>

								<ul class="contact__list">
									<li>
										<a href="http://www.website.com.br" title="">www.website.com.br</a>
									</li>
									<li>
										<i class="icon icon__phone--small"></i>
										<a href="tel:+5548999999999" title="">(48) 99999-9999</a>
									</li>
									<li>
										<i class="icon icon__locale"></i>
										<a href="" title="">Av xxx xxx xxx</a>
									</li>
								</ul>
							</div>
						</article>
					</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</section>

</main>

<?php require_once('../components/footer.php'); ?>