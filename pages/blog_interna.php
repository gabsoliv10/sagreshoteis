<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="blog blog__single">

		<div class="blog__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<section class="blog__listing">
							<article class="blog__post single__post">
								
								<div class="post__thumbnail">
									<img src="https://loremflickr.com/1190/700" alt="" title="">
								</div>

								<div class="post__details">

									<div class="post__info-wrapper">
										<div class="post__info">
											<h2 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
										</div>
									</div>

									<div class="post__excerpt">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ex tenetur facere ipsam consequatur, pariatur, iure neque dicta esse rem sunt aspernatur necessitatibus adipisci dolorem officia voluptatem quod sequi accusantium.

											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus necessitatibus suscipit molestiae eos culpa quaerat eligendi nemo nesciunt, eaque ullam, in dignissimos praesentium aut quas, architecto magnam earum voluptate ratione.
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ex tenetur facere ipsam consequatur, pariatur, iure neque dicta esse rem sunt aspernatur necessitatibus adipisci dolorem officia voluptatem quod sequi accusantium.

											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus necessitatibus suscipit molestiae eos culpa quaerat eligendi nemo nesciunt, eaque ullam, in dignissimos praesentium aut quas, architecto magnam earum voluptate ratione.
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet est ipsa nisi, repudiandae quam magni doloribus. Inventore accusantium magni laboriosam, perspiciatis dolor tempore est nostrum, voluptatum blanditiis doloremque nihil?
										</p>
									</div>

									<div class="post__share">
										<span>Compartilhe: <i class="fa fa-tag" aria-hidden="true"></i></span>
									</div>

								</div>

							</article>

							<div class="post__comments block__aside">

								<h3>Deixe sua resposta</h3>
								
								<form class="contact__form" method="POST" action="">

									<div class="form__fields">

										<p class="field__label">Todos os campos com <strong>*</strong> devem ser preenchidos</p>

										<div class="row">
											<div class="col-xs-12">
												<label for="mensagem" aria-labelledby="mensagem">
													<textarea name="" placeholder="Mensagem"></textarea>
												</label>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4">
												<label for="nome" aria-labelledby="nome">
													<input type="text" name="nome" placeholder="Nome">
												</label>
											</div>

											<div class="col-xs-12 col-sm-6 col-md-4">
												<label for="email" aria-labelledby="email">
													<input type="text" name="email" placeholder="E-mail">
												</label>
											</div>
										</div>

										<div class="form__actions">
											<input class="btn btn-primary" type="submit" value="Enviar">
										</div>

									</div>


								</form>

							</div>

						</section>
					</div>

				</div>

			</div>

		</div>


	</section>

</main>

<?php require_once('../components/footer.php'); ?>