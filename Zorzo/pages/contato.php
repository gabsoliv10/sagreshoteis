<?php require_once('../components/header.php'); ?>

<main class="main-contact" role="main">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-12">
				<h2 class="title">Contato</h2>

				<div class="contact__info">
					<p>Ligue para <a href="tel:+554832220061">048 3222-0061</a> ou preencha o formulario que entraremos em contato.</p>
					<a href="tel:+554899830553"><i class="fa fa-whatsapp"></i>  (48) 9983-0553</a>
				</div>

				<div class="contact__hour clearfix">
					<h2>Horário de <br/>Funcionamento</h2>

					<div class="col-md-6 col-sm-12">
						<h3>Segunda à Sexta</h3>
						<p>08:00 ao 12:00</p>
						<p>14:00 ao 18:45</p>
					</div>

					<div class="col-md-6 col-sm-12">
						<h3>Sábados e Domingos</h3>
						<p class="emergency">Atendimento Emergencial</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-md-offset-1 col-sm-12">
				<div class="contact__form">
					<form>
						<div class="row">
							<div class="col-xs-12">
								<h2>Use o formulário abaixo para entrar em contato.</h2>
							</div>
							<div class="col-md-6 col-sm-12">
								<input type="text" name="name" placeholder="Nome" />
								<input type="text" name="email" placeholder="E-mail" />
								<input type="text" name="tel" placeholder="Mensagem" />
							</div>
							<div class="col-md-6 col-sm-12">
								<textarea name="textarea" placeholder="Faça sua pré-consulta aqui" ></textarea>
								<input class="btn" type="submit" value="Enviar" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="map"></div>
	
</main>

<?php require_once('../components/footer.php'); ?>