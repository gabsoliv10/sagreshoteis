<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__hotels-v2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="hotels__listing">
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="hotel__item">

									<div class="col-xs-12 col-md-4 col-md-offset-1 pull-right">	
										<figure class="hotel__thumbnail">
											<img src="../assets/images/hoteis/ilha_madeira.jpg" title="" alt="">
										</figure>
									</div>

									<div class="col-xs-12 col-md-7">			
										<div class="hotel__details">
											<h2 class="hotel__name">Ilha da Madeira</h2>
											<p class="hotel__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam id adipisci quod possimus excepturi.</p>
											<div class="hotel__description">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos consequuntur dignissimos velit obcaecati nobis ipsa. Labore non, pariatur libero. Rem optio soluta quaerat, eum tempora necessitatibus odio omnis, voluptatibus quae.
														</p>

														<a class="item__link" href="hotel_interna.php"><i class="fa fa-angle-right"></i>Conheça o hotel</a>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam molestias quibusdam iusto at architecto, debitis quaerat autem eum, doloremque labore neque laudantium vel nemo voluptatibus facilis sint id dolores perspiciatis.
														</p>

														<a class="btn-call-to-book" href="contato.php">Reserve agora</a>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="hotel__item">

									<div class="col-xs-12 col-md-4 col-md-offset-1 pull-right">	
										<figure class="hotel__thumbnail">
											<img src="../assets/images/hoteis/acores.jpg" title="" alt="">
										</figure>
									</div>

									<div class="col-xs-12 col-md-7">			
										<div class="hotel__details">
											<h2 class="hotel__name">Hotel dos Açores</h2>
											<p class="hotel__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam id adipisci quod possimus excepturi.</p>
											<div class="hotel__description">
												<div class="row">
													<div class="col-xs-12 col-sm-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos consequuntur dignissimos velit obcaecati nobis ipsa. Labore non, pariatur libero. Rem optio soluta quaerat, eum tempora necessitatibus odio omnis, voluptatibus quae.
														</p>

														<a class="item__link" href="hotel_interna.php"><i class="fa fa-angle-right"></i>Conheça o hotel</a>
													</div>
													<div class="col-xs-12 col-sm-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam molestias quibusdam iusto at architecto, debitis quaerat autem eum, doloremque labore neque laudantium vel nemo voluptatibus facilis sint id dolores perspiciatis.
														</p>

														<a class="btn-call-to-book" href="contato.php">Reserve agora</a>

													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="hotel__item">

									<div class="col-xs-12 col-md-4 col-md-offset-1 pull-right">	
										<figure class="hotel__thumbnail">
											<img src="../assets/images/hoteis/praia_hotel.jpg" title="" alt="">
										</figure>
									</div>

									<div class="col-xs-12 col-md-7">			
										<div class="hotel__details">
											<h2 class="hotel__name">Sangres Praia Hotel</h2>
											<p class="hotel__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam id adipisci quod possimus excepturi.</p>
											<div class="hotel__description">
												<div class="row">
													<div class="col-xs-12 col-sm-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos consequuntur dignissimos velit obcaecati nobis ipsa. Labore non, pariatur libero. Rem optio soluta quaerat, eum tempora necessitatibus odio omnis, voluptatibus quae.
														</p>

														<a class="item__link" href="hotel_interna.php"><i class="fa fa-angle-right"></i>Conheça o hotel</a>
													</div>
													<div class="col-xs-12 col-sm-6">
														<p>
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam molestias quibusdam iusto at architecto, debitis quaerat autem eum, doloremque labore neque laudantium vel nemo voluptatibus facilis sint id dolores perspiciatis.
														</p>

														<a class="btn-call-to-book" href="contato.php">Reserve agora</a>

													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

</main>

<?php require_once('../components/footer.php'); ?>