<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__policy">

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">					

					<div class="policy__wrapper">
						<div class="policy__info">
							<h2 class="policy__section">Saiba mais ao se hospedar em nossos hoteís</h2>

							<ul class="policy__list">
								<?php for ($i=0; $i < 10; $i++): ?>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam mollitia maxime, voluptate officia consequuntur delectus! Maxime accusamus quos excepturi, accusantium quis, similique saepe rem numquam rerum tenetur hic eum vero!</li>
								<?php endfor; ?>
							</ul>

							<nav class="policy__links">
								<a href="">Lei N0 16.595, de 19 de janeiro de 2015 - referente a hospedagem de menor de idade</a>
								<a href="">Modelo Autorização de Hospedagem de Menor de Idade</a>
							</nav>
						</div>

						<div class="policy__cancel-methods">

							<h2 class="policy__section">Para cancelamento</h2>

							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="cancel__method">
										<h3>Para reservas de grupo</h3>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil obcaecati nostrum eaque unde exercitationem. Modi eveniet assumenda magni fuga, et ea exercitationem veniam debitis voluptatem quos perspiciatis, error autem quas?</p>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero quibusdam magni suscipit eveniet veritatis rem repellendus provident fugit animi accusamus nesciunt cupiditate amet, expedita itaque debitis omnis totam necessitatibus enim.</p>

									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="cancel__method">
										<h3>Para reservas individuais</h3>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam eos perferendis maxime aut incidunt corporis, reprehenderit suscipit, quaerat fugit, molestias unde dignissimos fuga doloremque eligendi ab quasi neque, ut aliquam.</p>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, quidem dolores, eaque tenetur molestiae explicabo distinctio sit amet, facilis accusantium nobis magnam velit animi! Necessitatibus vitae error in voluptates! Eos!</p>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</section>
</main>

<?php require_once('../components/footer.php'); ?>