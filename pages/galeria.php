<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__gallery block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<div class="gallery__listing">

						<div class="row">

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/categoria0-01.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

						</div>

					</div>

					<div class="section__actions">
						<a href="" title="Carregar Outras Fotos" class="btn btn-primary">Carregar Outras Fotos</a>
					</div>

				</div>
			</div>
		</div>
		
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>