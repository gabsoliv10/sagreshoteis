<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__contact">
		
		<div class="block__section">
			<header>
				<h1 class="section__title">Trabalhe conosco</h1>
			</header>
		</div>

		<div class="container">
			<div class="row no-margin">
				<div class="col-xs-12">
					
					<div class="form__block">
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 no-padding">								

								<form name="contactForm" class="contact__form" method="POST">

									<div class="form__fields">
										<div class="row">

											<div class="col-xs-12 col-md-12">
												<p  class="contact__select">Selecione o Hotel</p>
												<label for="nome" aria-labelledby="nome">
													<select name="">
														<option value="ilha_da_madeira">Ilha da Madeira</option>
														<option value="hotel_dos_acores">Hotel dos Açores</option>
														<option value="sangres_praia_hotel">Sangres Praia Hotel</option>
													</select>
												</label>
											</div>

											<div class="col-xs-12 col-md-12">
												<label for="nome" aria-labelledby="nome">
													<input type="text" name="nome" placeholder="Nome">
												</label>
											</div>

											<div class="col-xs-12 col-md-12">
												<label for="email" aria-labelledby="email">
													<input type="email" name="email" placeholder="E-mail">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="mensagem" aria-labelledby="mensagem">
													<textarea name="mensagem" placeholder="Fale sobre você"></textarea>
												</label>
											</div>
										</div>



										<div class="form__actions">
											<div class="row">
												<div class="col-xs-12">
													<div class="contact__file">
														<label for="file" aria-labelledby="file">
															Anexar arquivo:
														</label>
														<div class="upload-btn-wrapper">
															<button class="btn__file">Anexar arquivo</button>
															<input type="file" name="myfile" />
														</div>
													</div>
												</div>
											</div>
											<div class="contact__submit">
												<div class="contact__submit-number">
													<label for="number" aria-labelledby="number">8 + 39 </label>
													<input name="number" placeholder="?" type="number" value="Enviar">
												</div>
												<div>
													<input class="btn btn-primary" type="submit" value="Enviar">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	
	<div class="block__map">
		<div class="footer_line"></div>
	</div>

</main>

<div class="contact__footer">
<?php require_once('../components/footer.php'); ?>
	
</div>

